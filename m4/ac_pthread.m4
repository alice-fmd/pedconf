dnl ==================================================================
dnl
dnl Macro to check for thread flags
dnl
dnl AC_PTHREAD([ACTION-IF-FOUND[, ACTION-IF-NOT-FOUND]])
dnl
AC_DEFUN([AC_PTHREAD], 
[
  AC_REQUIRE([AC_CANONICAL_HOST])
  AC_LANG_SAVE
  AC_LANG_C
  ac_pthread_ok=no

  if test x"$PTHREAD_LIBS$PTHREAD_CFLAGS$PTHREAD_LDFLAGS" != x; then
    save_CFLAGS="$CFLAGS"
    save_LIBS="$LIBS"
    save_LDFLAGS="$LDFLAGS"
    CFLAGS="$CFLAGS $PTHREAD_CFLAGS"
    LIBS="$PTHREAD_LIBS $LIBS"
    LDFLAGS="$PTHREAD_LDFLAGS $LDFLAGS"
    AC_MSG_CHECKING([for pthread_join in LDFLAGS=$PTHREAD_LDFLAGS LIBS=$PTHREAD_LIBS with CFLAGS=$PTHREAD_CFLAGS])
    AC_TRY_LINK_FUNC(pthread_join, ac_pthread_ok=yes)
    AC_MSG_RESULT($ac_pthread_ok)
    if test x"$ac_pthread_ok" = xno; then
      PTHREAD_LIBS=""
      PTHREAD_CFLAGS=""
      PTHREAD_LDFLAGS=""
    fi
    LIBS="$save_LIBS"
    CFLAGS="$save_CFLAGS"
    LDFLAGS="$save_LDFLAGS"
  fi

  ac_pthread_flags="pthreads none -Kthread -kthread lthread -pthread -pthreads -mthreads pthread --thread-safe -mt pthread-config"

  # The ordering *is* (sometimes) important.  
  case "${host_cpu}-${host_os}" in
  *solaris*)
    ac_pthread_flags="-pthreads pthread -mt -pthread $ac_pthread_flags"
    ;;
  esac

  if test x"$ac_pthread_ok" = xno; then
    for flag in $ac_pthread_flags; do
      case $flag in
      none) AC_MSG_CHECKING([whether pthreads work without any flags]) ;;
      -*)   AC_MSG_CHECKING([whether pthreads work with $flag])
            PTHREAD_CFLAGS="$flag"
            PTHREAD_LDFLAGS="$flag"
            ;;
      pthread-config)
	AC_CHECK_PROG(ac_pthread_config, pthread-config, yes, no)
	if test x"$ac_pthread_config" = xno; then continue; fi
	PTHREAD_CFLAGS="`pthread-config --cflags`"
	PTHREAD_LIBS="`pthread-config --ldflags` `pthread-config --libs`"
	;;
      *) AC_MSG_CHECKING([for the pthreads library -l$flag]) 
         PTHREAD_LIBS="-l$flag"
         ;;
      esac

      save_LIBS="$LIBS"
      save_CFLAGS="$CFLAGS"
      save_LDFLAGS="$LDFLAGS"
      LIBS="$PTHREAD_LIBS $LIBS"
      CFLAGS="$CFLAGS $PTHREAD_CFLAGS"
      LDFLAGS="$CFLAGS $PTHREAD_LDFLAGS"

      AC_TRY_LINK([#include <pthread.h>],
                  [pthread_t th = pthread_self(); pthread_join(th, 0);
                   pthread_attr_init(0); pthread_cleanup_push(0, 0);
                   pthread_create(0,0,0,0); pthread_cleanup_pop(0); ],
                  [ac_pthread_ok=yes])
      LIBS="$save_LIBS"
      CFLAGS="$save_CFLAGS"
      LDFLAGS="$save_LDFLAGS"

      AC_MSG_RESULT($ac_pthread_ok)
      if test "x$ac_pthread_ok" = xyes; then
        break;
      fi

      PTHREAD_LIBS=""
      PTHREAD_CFLAGS=""
      PTHREAD_LDFLAGS=""
    done
  fi

  if test "x$ac_pthread_ok" = xyes; then
    save_LIBS="$LIBS"
    save_CFLAGS="$CFLAGS"
    save_LDFLAGS="$LDFLAGS"
    LIBS="$PTHREAD_LIBS $LIBS"
    CFLAGS="$CFLAGS $PTHREAD_CFLAGS"
    LDFLAGS="$LDFLAGS $PTHREAD_LDFLAGS"

    # Detect AIX lossage: JOINABLE attribute is called UNDETACHED.
    AC_MSG_CHECKING([for joinable pthread attribute])
    attr_name=unknown
    for attr in PTHREAD_CREATE_JOINABLE PTHREAD_CREATE_UNDETACHED; do
      AC_TRY_LINK([#include <pthread.h>], [int attr=$attr; return attr;],
                  [attr_name=$attr; break])
    done
    AC_MSG_RESULT($attr_name)
    if test "$attr_name" != PTHREAD_CREATE_JOINABLE; then
      AC_DEFINE_UNQUOTED(PTHREAD_CREATE_JOINABLE, $attr_name,
                        [Define to necessary symbol if this constant
                         uses a non-standard name on your system.])
    fi

    AC_MSG_CHECKING([if more special flags are required for pthreads])
    flag=no
    case "${host_cpu}-${host_os}" in
    *-aix* | *-freebsd* | *-darwin*) flag="-D_THREAD_SAFE";;
    *solaris* | *-osf* | *-hpux*)    flag="-D_REENTRANT"  ;;
    esac
    AC_MSG_RESULT(${flag})
    if test "x$flag" != xno; then
      PTHREAD_CFLAGS="$flag $PTHREAD_CFLAGS"
    fi

    LIBS="$save_LIBS"
    CFLAGS="$save_CFLAGS"
    LDFLAGS="$save_LDFLAGS"

    # More AIX lossage: must compile with cc_r
    AC_CHECK_PROG(PTHREAD_CC, cc_r, cc_r, ${CC})
  else
    PTHREAD_CC="$CC"
  fi

  AC_SUBST(PTHREAD_LIBS)
  AC_SUBST(PTHREAD_CFLAGS)
  AC_SUBST(PTHREAD_LDFLAGS)
  AC_SUBST(PTHREAD_CC)

  # Finally, execute ACTION-IF-FOUND/ACTION-IF-NOT-FOUND:
  AH_TEMPLATE(HAVE_PTHREAD,
              [Define if you have POSIX threads libraries and header files.])
  if test x"$ac_pthread_ok" = xyes; then
    ifelse([$1],,AC_DEFINE(HAVE_PTHREAD,1),[$1])
        :
  else
    ac_pthread_ok=no
    $2
  fi
  AC_LANG_RESTORE
])

