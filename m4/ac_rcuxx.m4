dnl __________________________________________________________________
dnl
dnl AC_RCUXX([MINIMUM-VERSION 
dnl             [,ACTION-IF_FOUND 
dnl              [, ACTION-IF-NOT-FOUND]])
AC_DEFUN([AC_RCUXX],
[
    # Command line argument to specify prefix. 
    AC_ARG_WITH([rcuxx],
        [AC_HELP_STRING([--with-rcuxx],	[Prefix where Rcu++ is installed])],
	[],[with_rcuxx="yes"])

    # Command line argument to specify documentation URL. 
    AC_ARG_WITH([rcuxx-url],
        [AC_HELP_STRING([--with-rcuxx-url],
		[Base URL where the Rcu++ dodumentation is installed])],
        rcuxx_url=$withval, rcuxx_url="")
    if test "x${RCUXX_CONFIG+set}" != xset ; then 
        if test "x$with_rcuxx" != "xno" ; then 
	    RCUXX_CONFIG=$with_rcuxx/bin/rcuxx-config
	fi
    fi   
	
    # Check for the configuration script. 
    if test "x$with_rcuxx" != "xno" ; then 
        AC_PATH_PROG(RCUXX_CONFIG, rcuxx-config, no)
        rcuxx_min_version=ifelse([$1], ,0.3,$1)
        # Message to user
        AC_MSG_CHECKING(for Rcu++ version >= $rcuxx_min_version)

        # Check if we got the script
        with_rcuxx=no    
        if test "x$RCUXX_CONFIG" != "xno" ; then 
           # If we found the script, set some variables 
           RCUXX_CPPFLAGS=`$RCUXX_CONFIG --cppflags`
           RCUXX_INCLUDEDIR=`$RCUXX_CONFIG --includedir`
           RCUXX_LIBS=`$RCUXX_CONFIG --libs`
           RCUXX_LTLIBS=`$RCUXX_CONFIG --ltlibs`
           RCUXX_LIBDIR=`$RCUXX_CONFIG --libdir`
           RCUXX_LDFLAGS=`$RCUXX_CONFIG --ldflags`
           RCUXX_LTLDFLAGS=`$RCUXX_CONFIG --ltldflags`
           RCUXX_PREFIX=`$RCUXX_CONFIG --prefix`
           
           # Check the version number is OK.
           rcuxx_version=`$RCUXX_CONFIG -V` 
           rcuxx_vers=`echo $rcuxx_version | \
             awk 'BEGIN { FS = "."; } \
    	   { printf "%d", ($''1 * 1000 + $''2) * 1000 + $''3;}'`
           rcuxx_regu=`echo $rcuxx_min_version | \
             awk 'BEGIN { FS = "."; } \
    	   { printf "%d", ($''1 * 1000 + $''2) * 1000 + $''3;}'`
           if test $rcuxx_vers -ge $rcuxx_regu ; then 
                with_rcuxx=yes
           fi
        fi
        AC_MSG_RESULT($with_rcuxx - is $rcuxx_version) 
    
        # Some autoheader templates. 
        AH_TEMPLATE(HAVE_RCUXX, [Whether we have rcuxx])
    
    
        if test "x$with_rcuxx" = "xyes" ; then
            # Now do a check whether we can use the found code. 
            save_LDFLAGS=$LDFLAGS
    	    save_CPPFLAGS=$CPPFLAGS
	    save_LIBS=$LIBS
	    LIBS="$LIBS $PTHREAD_LIBS $RCUXX_LIBS"
            LDFLAGS="$LDFLAGS $RCUXX_LDFLAGS"
            CPPFLAGS="$CPPFLAGS $RCUXX_CPPFLAGS"
     
            # Change the language 
            AC_LANG_PUSH(C++)
    
     	    # Check for a header 
            have_rcuxx_rcu_h=0
            AC_CHECK_HEADER([rcuxx/Rcu.h], [have_rcuxx_rcu_h=1])
    
            # Check the library. 
            have_librcuxx=no
            AC_MSG_CHECKING(for -lrcuxx)
            AC_LINK_IFELSE([AC_LANG_PROGRAM([#include <rcuxx/Rcu.h>],
                                            [Rcuxx::Rcu::Open("foo")])], 
                                            [have_librcuxx=yes])
            AC_MSG_RESULT($have_librcuxx)
    
            if test $have_rcuxx_rcu_h -gt 0    && \
                test "x$have_librcuxx"   = "xyes" ; then
    
                # Define some macros
                AC_DEFINE(HAVE_RCUXX)
            else 
                with_rcuxx=no
            fi
            # Change the language 
            AC_LANG_POP(C++)
	    CPPFLAGS=$save_CPPFLAGS
    	    LDFLAGS=$save_LDFLAGS
	    LIBS=$save_LIBS
        fi
    
        AC_MSG_CHECKING(where the Rcu++ documentation is installed)
        if test "x$rcuxx_url" = "x" && \
    	test ! "x$RCUXX_PREFIX" = "x" ; then 
           RCUXX_URL=${RCUXX_PREFIX}/share/doc/rcuxx/html
        else 
    	RCUXX_URL=$rcuxx_url
        fi	
        AC_MSG_RESULT($RCUXX_URL)
    fi
   
    if test "x$with_rcuxx" = "xyes" ; then 
        ifelse([$2], , :, [$2])
    else 
        ifelse([$3], , :, [$3])
    fi
    AC_SUBST(RCUXX_URL)
    AC_SUBST(RCUXX_PREFIX)
    AC_SUBST(RCUXX_CPPFLAGS)
    AC_SUBST(RCUXX_INCLUDEDIR)
    AC_SUBST(RCUXX_LDFLAGS)
    AC_SUBST(RCUXX_LIBDIR)
    AC_SUBST(RCUXX_LIBS)
    AC_SUBST(RCUXX_LTLIBS)
    AC_SUBST(RCUXX_LTLDFLAGS)
])
dnl
dnl EOF
dnl 
