dnl ------------------------------------------------------------------
dnl
dnl AC_ROOT_DIM([MINIMUM-VERSION 
dnl                   [,ACTION-IF_FOUND 
dnl                    [, ACTION-IF-NOT-FOUND]])
AC_DEFUN([AC_DIM],
[
    AC_REQUIRE([AC_PTHREAD])
    AC_ARG_WITH([dim-prefix],
        [AC_HELP_STRING([--with-dim-prefix],
		[Prefix where DIM is installed])],
        dim_prefix=$withval, dim_prefix="")

    if test "x${DIM_CONFIG+set}" != xset ; then 
        if test "x$dim_prefix" != "x" ; then 
	    DIM_CONFIG=$dim_prefix/bin/dim-config
	fi
    fi   
    if test "x${DIM_CONFIG+set}" != xset ; then 
        if test "x$dim_prefix" != "x" ; then 
	    DIM_CONFIG=$dim_prefix/bin/dim-config
	fi
    fi   
    AC_PATH_PROG(DIM_CONFIG, dim-config, no)
    dim_min_version=ifelse([$1], ,0.1,$1)
    
    AC_MSG_CHECKING(for DIM version >= $dim_min_version)

    dim_found=no    
    if test "x$DIM_CONFIG" != "xno" ; then 
       DIM_CFLAGS=`$DIM_CONFIG --cflags`
       DIM_INCLUDEDIR=`$DIM_CONFIG --includedir`
       DIM_CPPFLAGS=`$DIM_CONFIG --cppflags`
       if test ! "x$DIM_INCLUDEDIR" = "x" ; then 
	 DIM_CPPFLAGS="$DIM_CPPFLAGS -I$DIM_INCLUDEDIR"
       fi
       DIM_LIBS=`$DIM_CONFIG --libs`
       DIM_LIBDIR=`$DIM_CONFIG --libdir`
       DIM_LDFLAGS=`$DIM_CONFIG --ldflags`
       DIM_PREFIX=`$DIM_CONFIG --prefix`
       DIM_JNILIBS=`$DIM_CONFIG --libs`
       case $DIM_CFLAGS in 
	*-pthread*) DIM_LDFLAGS="$DIM_LDFLAGS -pthread" ;; 
       esac
       
       dim_version=`$DIM_CONFIG -V` 
       dim_vers=`echo $dim_version | \
         awk 'BEGIN { FS = " "; } \
	   { printf "%d", ($''1 * 1000 + $''2) * 1000 + $''3;}'`
       dim_regu=`echo $dim_min_version | \
         awk 'BEGIN { FS = " "; } \
	   { printf "%d", ($''1 * 1000 + $''2) * 1000 + $''3;}'`
       if test $dim_vers -ge $dim_regu ; then 
            dim_found=yes
       fi
    fi
    AC_MSG_RESULT($dim_found - is $dim_version) 
  
    if test "x$dim_found" = "xyes" ; then 
        ifelse([$2], , :, [$2])
    else 
        ifelse([$3], , :, [$3])
    fi
    AC_SUBST(DIM_PREFIX)
    AC_SUBST(DIM_CFLAGS)
    AC_SUBST(DIM_CPPFLAGS)
    AC_SUBST(DIM_INCLUDEDIR)
    AC_SUBST(DIM_LDFLAGS)
    AC_SUBST(DIM_LIBDIR)
    AC_SUBST(DIM_LIBS)
    AC_SUBST(DIM_JNILIBS)
])
