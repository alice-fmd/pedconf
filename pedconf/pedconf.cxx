#include "config.h"
#include <confdaemon/Options.h>
#include <rcuxx/Rcu.h>
#include <rcuxx/rcu/RcuACL.h>
#include <rcuxx/Altro.h>
#include <pedconf/PedLoader.h>
#ifndef VERSION
/** Version number */
# define VERSION "?.?"
#endif
#include <stdexcept>
#include "FindFile.cxx"


//____________________________________________________________________
int
main(int argc, char** argv)
{
  Option<bool>         hOpt('h', "help",      "\tThis help", false, false); 
  Option<bool>         vOpt('v', "version",   "\tShow version",false,false);
  Option<std::string>  dOpt('d', "debug",     "\tTurn on debug messages");
  Option<std::string>  iOpt('i', "input",     "\tInput file\t","");
  Option<std::string>  POpt('P', "path",      "\tSearch path\t","");
  Option<bool>         eOpt('e', "emulation", "\tEmulation",false,false);
  Option<bool>         VOpt('V', "verify",    "\tVerify data",false,false);
  Option<bool>         COpt('C', "check",     "\tCheck returns",false,false);
  Option<bool>         NOpt('N', "no-load",   "\tDo not Load pedestals",
			    false,false);
  Option<unsigned int> OOpt('O', "offset",    "\tFirst timebin offset",15);
  Option<unsigned int> MOpt('M', "max-lines", "\tMaximum number of lines",2048);
  Option<float>        FOpt('F', "factor",    "\tNoise factor",3);
  CommandLine cl("SOURCE");
  cl.Add(hOpt);
  cl.Add(vOpt);
  cl.Add(dOpt);
  cl.Add(iOpt);
  cl.Add(POpt);
  cl.Add(eOpt);
  cl.Add(VOpt);
  cl.Add(COpt);
  cl.Add(FOpt);
  cl.Add(NOpt);
  cl.Add(OOpt);
  cl.Add(MOpt);
  if (!cl.Process(argc, argv)) return 1;
  if (hOpt.IsSet()) {
    cl.Help();
    Rcuxx::Rcu::PrintHelp(std::cout);
    return 0;
  }
  if (vOpt.IsSet()) {
    std::cout << "pedconf, Rcu++ version " << VERSION << std::endl;
    return 0;
  }

  // --- Check whether the options actually means anything -----------
  if (NOpt.IsSet() && !VOpt.IsSet()) { 
    std::cout << "Option --no-load given, but no --verify, which means "
	      << "we have nothing to do - exiting with success" 
	      << std::endl;
    return 0;
  }

  // --- Check that we have a device ---------------------------------
  std::string device = (cl.Remain().size() > 0 ? cl.Remain()[0] : "");
  if (device.empty()) { 
    std::cerr << "No RCU interface specified, try " << argv[0] 
	      << " --help" << std::endl;
    return 1;
  }

  // --- Figure our debugging flags ----------------------------------
  std::ifstream* finput = 0;
  bool rcuxx_debug = dOpt->find("rcuxx")   != std::string::npos;
  bool bkend_debug = dOpt->find("backend") != std::string::npos; 
  bool pload_debug = dOpt->find("loader")  != std::string::npos; 
  bool ffind_debug = dOpt->find("find")    != std::string::npos;
  std::cout << "Rcu++ debug flag: " << rcuxx_debug << std::endl;

  // --- Open connection to RCU --------------------------------------
  Rcuxx::Rcu* rcu = Rcuxx::Rcu::Open(device.c_str(), eOpt.IsSet(),
				     rcuxx_debug);
  try {
    // --- Check for errors ------------------------------------------
    if (!rcu) throw std::runtime_error("Failed to open device");
    if (rcuxx_debug) rcu->SetDebug(Rcuxx::Rcu::kRcu, 1);
    if (bkend_debug) rcu->SetDebug(Rcuxx::Rcu::kBackend,1);

    // --- Get an ALTRO handler --------------------------------------
    Rcuxx::Altro altro(*rcu);

    // --- Set up for fake 'Echo' device -----------------------------
    if (device.find("echo:") != std::string::npos) { 
      Rcuxx::RcuACL* acl = rcu->ACL();
      if (!acl) 
	throw std::runtime_error("Failed to get ACL interface");
      unsigned short  boards[] = { 0x0, 0x1, 0x10, 0x11, 0xFFFF };
      unsigned short* board    = boards;
      while ((*board) != 0xFFFF) { 
	for (unsigned short altro = 0; altro < 3; altro++) { 
	  unsigned short mask = (altro == 1 ? 0xFF : 0xFFFF);
	  std::cout << "Enable " << std::hex << std::setfill('0') 
		    << "0x" << std::setw(2) << *board << "/" 
		    << "0x" << std::setw(1) << altro << " w/mask "
		    << "0x" << std::setw(4) << mask 
		    << std::dec << std::setfill(' ') << std::endl;
	  rcu->ACL()->EnableChip(*board, altro, mask);
	}
	board++;
      }
      rcu->ACL()->Commit();
    }

    // --- Check for input -------------------------------------------
    std::istream* input = &std::cin;
    if (iOpt.IsSet() && iOpt.Value() != "-") { 
      std::string file = iOpt.Value();
      std::string path = POpt.Value();
      if (!FindFile(file, path, ffind_debug)) {
	std::stringstream s;
	s << "Failed to find input file " << iOpt.Value()
          << " in path " << path;
	throw std::runtime_error(s.str().c_str());
      }
      std::cout << "Will read values from " << file << std::endl;
      finput = new std::ifstream(file.c_str());
      input  = finput;
    }
    else {
      std::cout << "Will read values from standard input" << std::endl;
    }

    // --- Make our loader -------------------------------------------
    PedConf::PedLoader loader(*rcu, altro, *input);
    loader.SetDebug(pload_debug);
    loader.SetVerify(VOpt.Value());
    loader.SetCheck(COpt.Value());
    loader.SetFactor(FOpt.Value());
    loader.SetNoload(NOpt.Value());
    loader.SetTOffset(OOpt.Value());
    loader.SetMaxLines(MOpt.Value());

    // --- Run the loader --------------------------------------------
    int ret = loader.Run();
    if (ret != 0) throw ret;

    // sleep(30);
  }
  // --- Error handling ----------------------------------------------
  catch (std::exception& e) { 
    std::cerr << e.what() << std::endl;
    return 1;
  }
  catch (int ret) {
    std::cerr << "Error # " << std::setw(3) << ret << ": ";
    if (rcu) std::cerr << rcu->ErrorString(-ret);
    std::cerr << std::endl;
    return 1;
  }
  catch (unsigned int ret) {
    std::cerr << "Error # " << std::setw(3) << ret << ": ";
    if (rcu) std::cerr << rcu->ErrorString(ret);
    std::cerr << std::endl;
    return 1;
  }
  if (finput) finput->close();
  
  return 0;
}
//
// EOF
// 
