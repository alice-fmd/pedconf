// -*- mode: C++ -*-
#ifndef PEDCONF_Daemon
#define PEDCONF_Daemon
#include <confdaemon/DaemonUtils.h>

namespace PedConf
{
  /** @brief Daemon running in the background and spawns pedconf
      application on demand.  */
  class Daemon : public DaemonUtils::Base
  {
  public:
    /** Constructor 
	@param dns     DNS node to use 
	@param name    String to append to base name for this server
	@param target  Target url for connecting to FEE
	@param file    The file to read Pedestal, Noise values from 
	@param path    Path to search input file for 
	@param bindir  Directory holding sub-process 
	@param pidfile File to append PID to 
	@param timeout Time-out execution of sub-process 
	@param retry   Whether to try more than once  
	@param debug   Enable debugging */
    Daemon(const std::string& dns, 
	   const std::string& name,
	   const std::string& target,
	   const std::string& file,
	   const std::string& path,
	   const std::string& bindir,
	   const std::string& pidfile,
	   unsigned int       timeout=60,
	   bool               retry=true, 
	   const std::string& debug=std::string());
    /** Destructor */
    ~Daemon();
    void Print(std::ostream& out) const;
  protected:
    /** Make the command line */ 
    void MakeCommandLine(const std::string& full, 
			 std::vector<std::string>& args);
    void SetChildReturn(int ret);
    void SetupAuxServices();
    /** The file name */ 
    const std::string fFile;
    /** Search path */ 
    const std::string fPath;
    /** Debug flags */ 
    const std::string fDebug;

    DimService* fRun;
    int fRunNo;
  };
}
#endif
//
// EOF
//


