#include "config.h"
#include <confdaemon/Options.h>
#include "Daemon.h"

int
main(int argc, char** argv)
{
  
  Option<bool>        hOpt('h', "help",     "Show this help", false,false);
  Option<bool>        VOpt('V', "version",  "Show version number",false,false);
  Option<bool>        rOpt('r', "retry",    "Retry command", false,false);
  Option<bool>        aOpt('a', "enable-done","Enable done state", false,false);
  Option<bool>        ROpt('R', "enable-recovery","Enable recovery state", 
			   false,false);
  Option<unsigned>    TOpt('T', "timeout",  "Timeout in seconds", 60);
  Option<std::string> DOpt('D', "dns",      "DIM DNS node", "alifmddimdns");
  Option<std::string> nOpt('n', "base",     "Prefix - ignored", "PEDCONF");
  Option<std::string> NOpt('N', "name",     "Name of FEE", "FMD-FEE_0_0_0");
  Option<std::string> pOpt('p', "pid",      "Pid file");
  Option<std::string> iOpt('i', "input",    "Name of pedestal file");
  Option<std::string> POpt('P', "path",     "Search path");
  Option<std::string> dOpt('d', "debug",    "Debug flags");
  Option<std::string> tOpt('t', "target",   "Interface");
  Option<std::string> BOpt('B', "bindir",   "Directory holding pedconf", 
			   BINDIR);
  Option<std::string> COpt('C', "reset",    "Reset RORC");
  CommandLine cl("");
  cl.Add(hOpt);
  cl.Add(VOpt);
  cl.Add(iOpt);
  cl.Add(POpt);
  cl.Add(dOpt);
  cl.Add(rOpt);
  cl.Add(NOpt);
  cl.Add(nOpt);
  cl.Add(DOpt);
  cl.Add(pOpt);
  cl.Add(tOpt);
  cl.Add(BOpt);
  cl.Add(TOpt);
  cl.Add(aOpt);
  cl.Add(ROpt);
  cl.Add(COpt);
  if (!cl.Process(argc, argv)) return 1;
  if (hOpt.IsSet()) {
    cl.Help();
    return 0;
  }
  if (VOpt.IsSet()) {
    std::cout << PACKAGE_STRING << std::endl;
    return 0;
  }

  std::string dns = DOpt;
  std::string nam = NOpt;
  std::string tgt = tOpt;
  std::string pid = pOpt;
  std::string bin = BOpt;
  std::string inp = iOpt;
  std::string pat = POpt;
  std::string dbg = dOpt;
  std::string rst = COpt;
  unsigned    tmo = TOpt;
  bool        rty = rOpt;
  bool        dne = aOpt;
  bool        rcy = ROpt;

  if (COpt.IsSet()) { 
    tgt.append("?reset");
    if (!rst.empty()) {
      tgt.append("=");
      tgt.append(rst);
    }
  }

  PedConf::Daemon daemon(dns, nam, tgt, inp, pat, bin, pid, tmo, rty, dbg);
  daemon.UseDone(dne);
  daemon.UseRecovery(rcy);
  daemon.Print(std::cout);
  std::cout << "\tPID file:      " << pid << std::endl;

  return daemon.Run();
}

//
// EOF
//

  
