#include "Daemon.h"
#include "config.h"
#include <cstdlib>
#include <stdexcept>
#include <sstream>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <unistd.h>
#include <cmath>
#include <cctype>
#include <algorithm>
#include <iterator>
#include <cstdarg>
#ifndef PACKAGE_URL
# define PACKAGE_URL "http://fmd.nbi.dk/fmd/fee/software.html"
#endif

//____________________________________________________________________
namespace 
{
  struct to_lower 
  {
    char operator()(char c) const { return std::tolower(c); }
  };
}

//____________________________________________________________________
PedConf::Daemon::Daemon(const std::string& dns, 
			const std::string& name, 
			const std::string& url, 
			const std::string& file,
			const std::string& path,
			const std::string& bindir,
			const std::string& pidfile,
			unsigned int       timeout,
			bool               retry,
			const std::string& debug)
  : DaemonUtils::Base("PEDCONF", dns, url, name, bindir, 
		      pidfile, timeout, retry), 
    fFile(file), 
    fPath(path),
    fDebug(debug), 
    fRun(0),
    fRunNo(0)
{
}

//____________________________________________________________________
void
PedConf::Daemon::SetupAuxServices()
{
  std::string rname;
  MakeServiceName("RUN", rname);

  fRun = new DimService(rname.c_str(), fRunNo);
}

//____________________________________________________________________
PedConf::Daemon::~Daemon()
{
  if (fRun) delete fRun;
}

//____________________________________________________________________
void
PedConf::Daemon::SetChildReturn(int ret)
{
  fRunNo = ret;
  fRun->setTimestamp(time(NULL), 0);
  fRun->updateService(fRunNo);
}

//____________________________________________________________________
void
PedConf::Daemon::Print(std::ostream& o) const
{
  o << PACKAGE_STRING    << "\n"
    << PACKAGE_URL       << "\n"
    << PACKAGE_BUGREPORT << std::endl;
  DaemonUtils::Base::Print(o);
  o << "\tPath:               " << fPath  << '\n'
    << "\tFile:               " << fFile  << '\n'
    << "\tDebug:              " << fDebug << std::endl;
}

  
#if 0
//____________________________________________________________________
namespace {
  template <typename T>
  bool
  GetValue(const char*        what, 
	  const std::string& str, 
	  T&                 val)
  {
    if  (str.find(what) == std::string::npos) return false;
    
    size_t eq = str.find("=");
    if (eq == std::string::npos) return true;
    
    std::string s(str.substr(eq+1, str.size()-eq-1));
    std::stringstream st(s);
    st >> val;
    return true;
  }
}
#endif

//____________________________________________________________________
void
PedConf::Daemon::MakeCommandLine(const std::string& tag, 
				 std::vector<std::string>& args)
{
  std::stringstream scmd;
  scmd << fBinDir << "/pedconf";
  args.push_back(scmd.str());
  args.push_back(fUrl);
  args.push_back("-i");
  args.push_back(fFile);

  if (tag.find("verify")  != std::string::npos) args.push_back("--verify");
  if (tag.find("check")   != std::string::npos) args.push_back("--check");
  if (tag.find("no-load") != std::string::npos) args.push_back("--no-load");

  float factor = -1;
  if (GetValue("factor", tag, factor) && factor > 0) { 
    std::stringstream sfac;
    sfac << "--factor=" << factor;
    args.push_back(sfac.str());
  }
  
  if (!fPath.empty()) { 
    args.push_back("-P");
    args.push_back(fPath);
  }
  if (!fDebug.empty()) { 
    args.push_back("-d");
    args.push_back(fDebug);
  }

}


//____________________________________________________________________
//
// EOF
//
