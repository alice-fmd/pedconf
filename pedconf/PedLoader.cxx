#include <pedconf/PedLoader.h>
#include <rcuxx/Rcu.h>
#include <rcuxx/Altro.h>
#include <rcuxx/rcu/RcuRCUID.h>
#include <rcuxx/rcu/RcuIMEM.h>
#include <rcuxx/rcu/RcuRMEM.h>
#include <rcuxx/rcu/RcuACL.h>
#include <rcuxx/rcu/RcuACTFEC.h>
#include <rcuxx/rcu/RcuTTCControl.h>
#include <rcuxx/rcu/RcuROIConfig1.h>
#include <rcuxx/rcu/RcuROIConfig2.h>
#include <rcuxx/rcu/RcuL1Timeout.h>
#include <rcuxx/rcu/RcuL2Timeout.h>
#include <rcuxx/rcu/RcuRoiTimeout.h>
#include <rcuxx/rcu/RcuL1MsgTimeout.h>
#include <rcuxx/rcu/RcuALTROCFG1.h>
#include <rcuxx/rcu/RcuTRGCONF.h>
#include <rcuxx/rcu/RcuALTROIF.h>
#include <rcuxx/rcu/RcuALTROCFG1.h>
#include <rcuxx/rcu/RcuALTROCFG2.h>
#include <rcuxx/altro/AltroPMDTA.h>
#include <rcuxx/altro/AltroPMADD.h>
#include <rcuxx/DebugGuard.h>
#include <rcuxx/RegisterGuard.h>
#include <iostream>
#include <sstream>
#include <string>
#include <stdexcept>
#include <iterator>
#include <cstdarg>
#include <cstdio>
#include <ctime>


//====================================================================
PedConf::PedLoader::PedLoader(Rcuxx::Rcu& rcu, Rcuxx::Altro& altro, 
			      std::istream& input)
  : fRunNo(0), 
    fRcu(rcu),
    fAltro(altro),
    fInput(input), 
    fFactor(0),
    fDebug(false),
    fVerify(false),
    fChecks(false), 
    fShowDisabled(false),
    fNoload(false),
    fTOffset(15),
    fMaxLines(2048), 
    fMinTimebin(kNBoard*kNAltro*kNChannel),
    fMaxTimebin(kNBoard*kNAltro*kNChannel)
{
  std::fill(fMinTimebin.begin(), fMinTimebin.end(), 1024);
  std::fill(fMaxTimebin.begin(), fMaxTimebin.end(),    0);
  
  // Get the active channel interface 
  Rcuxx::RcuACL* acl = fRcu.ACL();
  if (!acl) 
    throw std::runtime_error("No ACL interface defined for this RCU!");
  
  if (!fAltro.PMDTA()) 
    throw std::runtime_error("No PMDTA interface defined for ALTRO");
  if (!fAltro.PMADD()) 
    throw std::runtime_error("No PMADD interface defined for ALTRO");

  // Read from the front-end, the set active channels 
  unsigned int ret = acl->Update();
  if (ret) throw ret;  
}

#define DO_VARIADIC(BUF,N,MSG) \
 va_list ap; va_start(ap,MSG); vsnprintf(BUF,N,MSG,ap); va_end(ap)

//____________________________________________________________________
std::string
PedConf::PedLoader::Timestamp() const
{
  time_t     now     = time(NULL);
  struct tm* loctime = localtime(&now);
  char tbuf[256];
  strftime(tbuf, 256, "%d %b %Y %T", loctime);
  return std::string(tbuf);
}



//____________________________________________________________________
void
PedConf::PedLoader::Error(const char* msg, ...)
{
  static char buf[1024];
  DO_VARIADIC(buf, 1024, msg);
  std::cerr << "E [" << Timestamp() << "]: " << buf << std::flush;
}
//____________________________________________________________________
void
PedConf::PedLoader::Warn(const char* msg, ...)
{
  static char buf[1024];
  DO_VARIADIC(buf, 1024, msg);
  std::cerr << "W [" << Timestamp() << "]: " << buf << std::flush;
}
//____________________________________________________________________
void
PedConf::PedLoader::Info(const char* msg, ...)
{
  static char buf[1024];
  DO_VARIADIC(buf, 1024, msg);
  std::cout << "I [" << Timestamp() << "]: " << buf << std::flush;
}

#define PP(T,X) std::setw(50) << T << ": " << X << '\n' 
#define PU(T,X,U) std::setw(50) << T << ": " << X << U << '\n'

//____________________________________________________________________
void
PedConf::PedLoader::PrintSettings() const
{
  std::ostream& o = std::cout;
  o << "[" << Timestamp() << "]: Setings for this configuration:\n"
    << std::boolalpha 
    << std::left 
    << PP("Run number",                         fRunNo)
    << PP("Noise factor",                       fFactor)
    << PP("Debug",                              fDebug)
    << PP("Verify",                             fVerify)
    << PP("Do checks",                          fChecks)
    << PP("Log disabled",                       fShowDisabled)
    << PP("Do not actually load",               fNoload)
    << PP("Timebin offset",                     fTOffset)
    << PP("Maximum number of lines per commit", fMaxLines)
    << std::noboolalpha 
    << std::right 
    << std::flush;
}

//____________________________________________________________________
int
PedConf::PedLoader::Run()
{
  PrintSettings();
  if (fNoload && !fVerify) { 
    // If no-load is set, and we are not asked to verify, exit
    return 0;
  }		

  // Reset FSMs in RCU 
  try { 
    // Sicne RCU_RESET also resets the TTC register, we 
    // need to cache these here.  We do that using a 
    // guard pattern.  The guards throw execptions on 
    // error, so we need to do this in a try- catch block 
    Rcuxx::RegisterGuard g1(fRcu.TTCControl());
    Rcuxx::RegisterGuard g2(fRcu.ROIConfig1());
    Rcuxx::RegisterGuard g3(fRcu.ROIConfig2());
    Rcuxx::RegisterGuard g4(fRcu.L1Timeout());
    Rcuxx::RegisterGuard g5(fRcu.L2Timeout());
    Rcuxx::RegisterGuard g6(fRcu.RoiTimeout());
    Rcuxx::RegisterGuard g7(fRcu.L1MsgTimeout());
    Rcuxx::RegisterGuard g8(fRcu.TRGCONF());
    Rcuxx::RegisterGuard g9(fRcu.ALTROIF());
    Rcuxx::RegisterGuard g10(fRcu.ALTROCFG1());
    Rcuxx::RegisterGuard g11(fRcu.ALTROCFG2());

    fRcu.RCU_RESET()->Commit();
  }
  catch (const std::exception& e)  {
    Error("Got an exception: %s", e.what());
    return 1;
  }

  // Get the IMEM content
  Rcuxx::RcuIMEM::RestoreGuard rg(fRcu.IMEM(), true, true);

  Rcuxx::RcuALTROCFG1* altrocfg1 = fRcu.ALTROCFG1();
  if (altrocfg1) { 
    altrocfg1->Update();
    altrocfg1->SetSecondBPost(int(fFactor) & 0x7);
    altrocfg1->Commit();
  }

  unsigned id = 0;
  Rcuxx::RcuRCUID* rcuid = fRcu.RCUID();
  if (rcuid) { 
    rcuid->Update();
    id = rcuid->Id();
    rcuid->SetId((id & 0xf) | 1 << 4);
    rcuid->Commit();
  }

  PedTimebin::fDebug = fDebug;
  PedChannel::fDebug = fDebug;
  int ret = 0;
  try { 
    if ((ret = ReadInput())) throw ret;
    if ((ret = WritePeds())) throw ret;
    if ((ret = Verify()))    throw ret;
  }
  catch (int r) { 
    ret = r;
  }
  if (rcuid) { 
    rcuid->SetId((id & 0xF) | (2 << 4));
    rcuid->Commit();
  }

  if (ret) Error("PedLoader failed! (return %d)\n", ret);
  else     Info("PedLoader succeeded\n");
  return ret;
}

//____________________________________________________________________
int
PedConf::PedLoader::ReadInput()
{
  Rcuxx::DebugGuard g(fDebug, "Reading input");
  std::cout << "Reading input" << std::endl;
  // Get pointers to the ALTRO PMDAT and PMADD interfaces 
  Rcuxx::RcuACL*  acl = fRcu.ACL();
  
  // Line counter 
  unsigned int   lineno  = 0;

  // Loop until the end of the input of the passed stream
  while (!fInput.eof())  { 
    // Get a line from the stream 
    std::string line;
    std::getline(fInput, line);
    lineno++;
    // Rcuxx::DebugGuard::Message(fDebug, "line # %4d: %s", 
    //                            lineno, line.c_str());
    if (fInput.eof()) { 
      Info("At end of input @ line %d\n", lineno);
      break;
    }
    if (fInput.fail()) { 
      Error("Failed to read line %d from input\n", lineno);
      return 1;
    }

    // Check if the line is a comment 
    size_t first = line.find_first_not_of(" \t");
    if (first == std::string::npos) 
      // Empty line, continue 
      continue;

    if (line[first] == '#') 
      // Comment line - continue
      continue;
    
    if (line.find("run:") != std::string::npos) { 
      std::stringstream s(line);
      std::string rn;
      s >> rn >> fRunNo;
      continue;
    }

    // we should have a valid line, read it in 
    unsigned short board, chip, channel, timebin;
    float ped, noise;
    char c[5] = { '\0', '\0', '\0', '\0', '\0' };

    std::stringstream s(line);
    s >> board    >>  c[0]
      >> chip     >>  c[1]
      >> channel  >>  c[2]
      >> timebin  >>  c[3]
      >> ped      >>  c[4]
      >> noise;
    for (size_t i = 0; i < 5; i++) { 
      if (c[i] != ',') { 
	Warn("Line %d contains errors\n", lineno+1);
	continue;
      }
    }
    Rcuxx::DebugGuard::Message(fDebug, "Read 0x%02x/0x%x/0x%x/%04d: %f+/-%f "
			       "(line %d: %s)",
			       board, chip, channel, timebin, ped, noise, 
			       lineno, line.c_str());
 
    // Check if the channel is enabled 
    if (!acl->CheckChannel(board, chip, channel)) {
      if (fShowDisabled) 
	Warn("ALTRO channel 0x%02x/0x%1x/0x%02x is not enabled\n", 
	     board, chip, channel);
      continue;
    }

    int idx = HWIndex(board, chip, channel);
    if (idx < 0) { 
      Warn("Invalid hardware index for 0x%02x/0x%1x/0x%02x: %d\n", 
	   board, chip, channel, idx);
      continue;
    }
    
    unsigned short minT = std::min(fMinTimebin[idx], timebin);
    unsigned short maxT = std::max(fMaxTimebin[idx], timebin);
#if 0
    Rcuxx::DebugGuard::Message(true, "Time-bin range updated for "
			       "0x%02x/0x%x/0x%x [%6d]: %4d-%4d",
			       board, chip, channel, idx, minT, maxT);
#endif
    fMinTimebin[idx] = minT;
    fMaxTimebin[idx] = maxT;
    
    // See of the container exists - otherwise create it. 
    PedVector::iterator i = fPeds.find(timebin);
    if (i == fPeds.end()) {
      Rcuxx::DebugGuard::Message(fDebug, "New timebin: %4d", timebin);
      fPeds[timebin] = new PedTimebin(timebin);
    }
    

    fPeds[timebin]->AddChannel(board, chip, channel, 
			      short(ped + fFactor * noise));
  } // End of loop over input.
  Info("Filling data structures\n");

  unsigned short  boards[] = { 0x0, 0x1, 0x10, 0x11, 0xFFFF };
  unsigned short* board    = boards;
  while ((*board) != 0xFFFF) { 
    for (unsigned short altro = 0; altro < kNAltro; altro++) { 
      for (unsigned short channel = 0; channel < kNChannel; channel++) { 
	int idx = HWIndex(*board, altro, channel);
	if (idx < 0) { 
	  Warn("Invalid hardware index for 0x%02x/0x%1x/0x%02x: %d\n", 
	       board, altro, channel, idx);
	  continue;
	}
	unsigned short minT = fMinTimebin[idx];
	unsigned short maxT = fMaxTimebin[idx];

	// Skip stuff we haven't seen 
	if (minT > maxT) continue;

	Rcuxx::DebugGuard::Message(fDebug, "Time-bin range seen "
				   "0x%02x/0x%x/0x%x: %4d-%4d",
				   *board, altro, channel, minT, maxT);
	for (unsigned short t = fTOffset; t < minT; t++) { 
	  if (!fPeds[t]) {
	    Rcuxx::DebugGuard::Message(fDebug, "New timebin: %4d", t);
	    fPeds[t] = new PedTimebin(t);
	  }
	  fPeds[t]->AddChannel(*board, altro, channel, 0x3FF);
	}
	for (unsigned short t = maxT+1; t < 1024; t++) { 
	  if (!fPeds[t]) {
	    Rcuxx::DebugGuard::Message(fDebug, "New timebin: %4d", t);
	    fPeds[t] = new PedTimebin(t);
	  }
	  fPeds[t]->AddChannel(*board, altro, channel, 0x3FF);
	} // maxT
      } // channel
    } // altro
    board++;
  } // board

  Info("Input read and stored\n");
  return 0;
}


//____________________________________________________________________
int
PedConf::PedLoader::WritePeds()
{
  if (fNoload) return 0;
  Rcuxx::DebugGuard g(fDebug, "Writing pedestals");
  Rcuxx::RcuIMEM* imem = fRcu.IMEM();

  // Now, loop over the defined pedestal values, and write to
  // hardware. 
  int ret = 0;

  fFirst = fPeds.begin();
  unsigned int j = 0;
  Info("Will write a grand total of %d timebins\n", fPeds.size());
  for (PedIter i = fPeds.begin(); i != fPeds.end(); ++i, j++) {
    if ((imem->Size() - imem->Marker()) <= fMaxLines) { 
      // End this block, and commit to hardware 
      fLast = i;
      if ((ret = ExecIMEM(false, j))) return ret;
      fFirst = fLast;
    }

    // Skip the first ones. 
    if (i->second->fTimebin < fTOffset) continue;

    Rcuxx::DebugGuard::Message(fDebug, 
			       "Writing timebin %4d ", i->second->fTimebin);

    // Write the instructions 
    // std::cout << "Writing for " << i->second.Name() << std::endl;
    if ((ret = i->second->Write(fAltro,fTOffset)) < 0) return ret;
    ret = 0;
  }
  // Check if we have a pending write 
  if (fFirst != fPeds.end()) {
    fLast = fPeds.end();
    if ((ret = ExecIMEM(false, j))) return ret;
  }
  Info("Wrote pedestals\n");
  // And so we are done
  return ret;
}
//____________________________________________________________________
int
PedConf::PedLoader::Verify()
{
  if (!fVerify) return 0;
  Info("Will verify written data\n");
  Rcuxx::DebugGuard g(fDebug, "Verifying written values");

  Rcuxx::RcuIMEM* imem = fRcu.IMEM();
  imem->ClearMarker();
  // Now, loop over the defined pedestal values, and write to
  // hardware. 
  // const unsigned int max_lines = 1024; // 2 + 16 * 2;
  int ret = 0;

  fFirst = fPeds.begin();
  unsigned int j = 0;
  for (PedIter i = fPeds.begin(); i != fPeds.end(); ++i, j++) {
    if ((imem->Size() - imem->Marker()) <  fMaxLines) { 
      // End this block, and commit to hardware 
      fLast = i;
      if ((ret = ExecIMEM(true, j))) return ret;
      fFirst = fLast;
    }

    // Skip the first ones. 
    if (i->second->fTimebin < fTOffset) continue;

    // Write the instructions 
    if ((ret = i->second->Read(fAltro,fTOffset)) < 0) return -ret;
    ret = 0;
  }
  // Check if we have a pending write 
  if (fFirst != fPeds.end()) {
    fLast = fPeds.end();
    if ((ret = ExecIMEM(true, j))) return ret;
  }
  Info("Verified\n");
  // And so we are done
  return 0;
}

//____________________________________________________________________
int
PedConf::PedLoader::ExecIMEM(bool rnw, int j)
{
  Rcuxx::DebugGuard g(fDebug, "Executing IMEM for %s", rnw ? "read" : "write");
  Rcuxx::RcuIMEM* imem = fRcu.IMEM();
  Info("Execute at %d/%d [marker=%d]%c",
       j, fPeds.size(), imem->Marker(), (fDebug ? '\n' : '\r'));
  
  int ret  = 0;
  int dret = 0;
  if ((ret = imem->Execute())) return ret;

#if 0
  if (fDebug) { 
    USLEEP(10000);
    imem->Update();
    imem->Print();
  }
#endif

  if (!fChecks && !fVerify) return 0;
  USLEEP(10000);

  Rcuxx::RcuRMEM* rmem = fRcu.RMEM();
  if ((ret = rmem->Update())) return ret;
  // if (fDebug) rmem->Print();
 
  unsigned int off = 0;
  for (PedIter cur = fFirst; cur != fLast; ++cur) {
    // std::cout << "Validate " << cur->second.Name() << std::endl;
    if (cur->second->fTimebin < fTOffset) continue;
    if ((ret = cur->second->CheckReturn(rnw, fAltro, *rmem, off, fTOffset))) {
      Error("@ offset %d in RMEM [%d]: %s\n", \
	    off, ret, fRcu.ErrorString(ret).c_str());
      dret = ret;
    }
  }
  // rmem->Update();
  // rmem->Print();
  return dret;
}

//____________________________________________________________________
//
// EOF
//
