#include <rcuxx/Rcu.h>
#include <rcuxx/Altro.h>
#include <rcuxx/rcu/RcuIMEM.h>
#include <rcuxx/rcu/RcuRMEM.h>
#include <rcuxx/rcu/RcuACL.h>
#include <rcuxx/rcu/RcuACTFEC.h>
#include <rcuxx/altro/AltroPMDTA.h>
#include <rcuxx/altro/AltroPMADD.h>
#include <rcuxx/DebugGuard.h>
#include <pedconf/PedTimebin.h>
#include <iomanip>
#include <iostream>
#include <sstream>

bool PedConf::PedTimebin::fDebug = false;
bool PedConf::PedChannel::fDebug = false;

//====================================================================
PedConf::PedChannel::PedChannel(unsigned short b, 
				unsigned short a,
				unsigned short c,
				unsigned short p)
  : fBoard(b), fChip(a), fChannel(c), fPed(p)
{
}
//____________________________________________________________________
PedConf::PedChannel::PedChannel(const PedChannel& other) 
  : fBoard(other.fBoard), fChip(other.fChip), 
    fChannel(other.fChannel), fPed(other.fPed)
{
}
//____________________________________________________________________
PedConf::PedChannel&
PedConf::PedChannel::operator=(const PedChannel& other) 
{
  fBoard   = other.fBoard;
  fChannel = other.fChip;
  fChip    = other.fChannel;
  fPed     = other.fPed;
  return *this;
}
//____________________________________________________________________
int
PedConf::PedChannel::Write(Rcuxx::AltroPMDTA& pmdta)
{
  static int ncalls = 0;
  ncalls++;
  Rcuxx::DebugGuard g(fDebug, 
		      "Will write channel 0x%02x/0x%x/0x%x w/%4d to ALTRO", 
		      fBoard, fChip, fChannel, fPed);
		      
  // The pointers have already been checked. 
  int                ret   = 0;
  // Set the channel specific address
  pmdta.SetAddress(fBoard, fChip, fChannel);
  pmdta.SetPMDTA(fPed); 
  pmdta.Set(); // Sync
  if ((ret = pmdta.WriteInstructions())) return -ret;
  return 2;
}

//____________________________________________________________________
int
PedConf::PedChannel::Read(Rcuxx::AltroPMDTA& pmdta)
{
  // The pointers have already been checked. 
  int                ret   = 0;
  
  // Set the channel specific address 
  pmdta.SetAddress(fBoard, fChip, fChannel);
  pmdta.Set();
  if ((ret = pmdta.ReadInstructions())) return -ret;
  return 1;
}

//____________________________________________________________________
int
PedConf::PedChannel::CheckReturn(bool               rnw, 
				 Rcuxx::AltroPMDTA& pmdta, 
				 Rcuxx::RcuRMEM&    rmem, 
				 unsigned int&      off)
{
  // Return value 
  int ret = 0;
  int dret = 0;
  // Write the timebin address across all channels 
  unsigned int loff = off;
  pmdta.SetAddress(fBoard, fChip, fChannel);
  unsigned int full = pmdta.FullAddress();
  off += 2;
  if (!rnw) { 
    if ((ret = rmem.FecWrite(full, fPed, loff))) { 
      // std::cout << std::endl;
      unsigned int rAddr = rmem.Data()[loff];
      unsigned int rData = rmem.Data()[loff+1];
      std::cerr << "Failed to validate PMDTA write for " 
		<< std::setfill('0') << std::hex 
		<< std::setw(2) << fBoard   << "/" 
		<< std::setw(1) << fChip    << "/" 
		<< std::setw(1) << fChannel << " @ " 
		<< std::dec << std::setfill(' ') << loff 
		<< " with ped=" << fPed << "\n"
		<< std::setfill('0') << std::hex 
		<< " address returned " << (rAddr & 0xFFFFF) 
		<< " expected " << (full & 0xFFFFF) << "\n"
		<< " data returned " << (rData & 0xFFFFF) 
		<< " expected " << (fPed & 0xFFFFF) << std::endl;
      dret = ret;
    }
  }
  else { 
    unsigned int ped = 0;
    if ((ret = rmem.FecRead(full, ped, loff))) { 
      std::cout << std::flush;
      std::cerr << "Failed to validate PMDTA read" << std::endl;
      dret = ret;
    }
    short lped = ped;
    if (lped != fPed) {
      std::cout << std::flush;
      std::cerr << "Failed to validate PMDTA for "  
		<< std::setfill('0') << std::hex << "Pedestal for 0x" 
		<< std::setw(2) << fBoard   << "/"
		<< std::setw(1) << fChip    << "/" 
		<< std::setw(1) << fChannel << " read @ " << loff 
		<< " got " << ped << " expected " << fPed 
		<< std::endl;
      dret = 1;
    }
  }
  loff = off;
  if (dret) { 
    std::cerr << "Failed to validate channel " << (rnw ? "read" : "write") 
	      << std::endl;
    return dret;
  }
  return ret;
}



//====================================================================
PedConf::PedTimebin::PedTimebin(unsigned short t)
  : fTimebin(t)
{
  Reset();
}

//____________________________________________________________________
PedConf::PedTimebin::PedTimebin(const PedTimebin& p)
  : fTimebin(p.fTimebin)
{
  for (ChannelMap::const_iterator i = p.fChannels.begin(); 
       i != p.fChannels.end(); ++i) { 
    unsigned short addr = i->second->GetAddress();
    fChannels[addr] = new PedChannel(*(i->second));
  }
}
//____________________________________________________________________
PedConf::PedTimebin&
PedConf::PedTimebin::operator=(const PedTimebin& p)
{
  fTimebin  = p.fTimebin;
  for (ChannelMap::const_iterator i = p.fChannels.begin(); 
       i != p.fChannels.end(); ++i) { 
    unsigned short addr = i->second->GetAddress();
    fChannels[addr] = new PedChannel(*(i->second));
  }
  return *this;
}

//____________________________________________________________________
void
PedConf::PedTimebin::Reset()
{
  fChannels.clear();
}  
//____________________________________________________________________
void
PedConf::PedTimebin::Zero()
{
  for (ChannelMap::iterator i = fChannels.begin(); i != fChannels.end(); ++i) {
    PedChannel* p = i->second;
    p->fPed = 0x3ff;
  }
}  

//____________________________________________________________________
bool
PedConf::PedTimebin::HasChannel(unsigned short b, 
				unsigned short a, 
				unsigned short c) const
{ 
  unsigned short addr = PedChannel::Address(b, a, c);
  return fChannels.find(addr) != fChannels.end();
}
//____________________________________________________________________
void
PedConf::PedTimebin::AddChannel(unsigned short b, 
				unsigned short a,
				unsigned short c, 
				unsigned short p) 
{ 
  if (HasChannel(b,a, c)) return;
  unsigned short  addr     = PedChannel::Address(b, a, c);
#ifndef USE_STACK
  fChannels[addr] = new PedChannel(b, a, c, p);
#else
  fChannels[addr].fBoard   = b;
  fChannels[addr].fChip    = a;
  fChannels[addr].fChannel = c;
  fChannels[addr].fPed     = p;
#endif
}
//____________________________________________________________________
PedConf::PedChannel*
PedConf::PedTimebin::GetChannel(unsigned short b, 
				unsigned short a,
				unsigned short c) 
{ 
  unsigned short  addr     = PedChannel::Address(b, a, c);
  if (!HasChannel(b, a, c)) return 0;
  return fChannels[addr];
}
//____________________________________________________________________
int
PedConf::PedTimebin::Write(Rcuxx::Altro& altro, unsigned short off)
{
  Rcuxx::DebugGuard g(fDebug, "Will write timebin %4d to ALTRO", fTimebin);
		      
  // Counter of how many lines of IMEM we take up. 
  int cnt = 0;
  int ret = 0;

  // The pointers have already been checked. 
  Rcuxx::AltroPMADD* pmadd = altro.PMADD();
  Rcuxx::AltroPMDTA* pmdta = altro.PMDTA();
  
  // Write the timebin address across all channels 
  pmadd->SetBroadcast();
  pmadd->SetPMADD(fTimebin-off);
  pmadd->Set();
  if ((ret = pmadd->WriteInstructions())) return -ret;
  cnt += 2;

  for (ChannelMap::iterator i = fChannels.begin(); i != fChannels.end(); ++i) {
    int ret = i->second->Write(*pmdta);
    if (ret < 0) return ret;
    cnt += ret;
  }
  return cnt;
}

//____________________________________________________________________
int
PedConf::PedTimebin::Read(Rcuxx::Altro& altro, unsigned short off)
{
  // Counter of how many lines of IMEM we take up. 
  int cnt = 0;
  int ret = 0;

  // The pointers have already been checked. 
  Rcuxx::AltroPMADD* pmadd = altro.PMADD();
  Rcuxx::AltroPMDTA* pmdta = altro.PMDTA();
  
  // Write the timebin address across all channels 
  pmadd->SetBroadcast();
  pmadd->SetPMADD(fTimebin-off);
  pmadd->Set();
  if ((ret = pmadd->WriteInstructions())) return -ret;
  cnt += 1;

  for (ChannelMap::iterator i = fChannels.begin(); i != fChannels.end(); ++i) {
    int ret = i->second->Read(*pmdta);
    if (ret < 0) return ret;
    cnt += ret;
  }
  return cnt;
}

//____________________________________________________________________
int
PedConf::PedTimebin::CheckReturn(bool             rnw, 
				 Rcuxx::Altro&    altro, 
				 Rcuxx::RcuRMEM&  rmem, 
				 unsigned int&    off,
				 unsigned short   toff)
{
  // Return value 
  int ret = 0;
  int dret = 0;
  // The pointers have already been checked. 
  Rcuxx::AltroPMADD* pmadd = altro.PMADD();
  Rcuxx::AltroPMDTA* pmdta = altro.PMDTA();
  
  // Write the timebin address across all channels 
  pmadd->SetBroadcast();
  unsigned int full = pmadd->FullAddress();
  unsigned int loff = off;
  off += 2;
  if ((ret = rmem.FecWrite(full, fTimebin-toff, loff))) { 
    std::cout << std::flush;
    std::cerr << "Failed to validate PMADD write - expected " 
	      << fTimebin << "-" << toff << "=" 
	      << fTimebin-toff << std::endl;
    dret = ret;
  }
  loff = off;

  for (ChannelMap::iterator i = fChannels.begin(); i != fChannels.end(); ++i) {
    int ret = i->second->CheckReturn(rnw, *pmdta, rmem, loff);
    if (ret) dret = ret;
    off = loff;
  }
  if (dret) { 
    std::cerr << "Failed to validate timebin " << fTimebin << " write" 
	      << std::endl;
    return dret;
  }
  return ret;
}
  
//
// EOF
//
