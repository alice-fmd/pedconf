#!/bin/sh

type=flat
start=0
end=544
verbose=0
value=100
noise=0

usage()
{
    cat <<EOF
Usage: $0 [OPTIONS]

Write a fake pedestal file to standard output

Options: 

	-h,--help		Show this help
	-s,--start=TIMEBIN	Set start timebin             [$start]
	-e,--end=TIMEBIN	Set end timebin               [$end]
	-t,--type=TYPE		Set type of pedestals         [$type]
	-p,--value=NUM		Base or fixed pedestal value  [$value]
	-n,--noise=NUM		Fixed noise		      [$noise]
	-v,--verbose		Be verbose		      [$verbose]
EOF
}

while test $# -gt 0 ; do 
    case $1 in 
	--*=*) 
	    opt=`echo $1 | sed 's/=.*//'` 
	    arg=`echo $1 | sed 's/.*=//'` 
	    maybe=0
	    ;;
	--*)
	    opt=$1 
	    arg=
	    maybe=0
	    ;;
	-*)
	    opt=$1 
	    arg=$2 
	    maybe=1
	    ;;
	*)
	    opt=
	    arg=
    esac
    shift 

    case $opt in 
	-h|--help)    usage; exit 0 ;; 
	-t|--type)    type=`echo $arg | tr '[A-Z]' '[a-z]'` ;;
	-s|--start)   start=$arg ;;
	-e|--end)     end=$arg ;;
	-p|--value)   value=$arg ;;
	-n|--noise)   noise=$arg ;;
	-v|--verbose) verbose=1 ; maybe=0 ;; 
	*)
	    echo "Unknown argument '$opt', try $0 --help" > /dev/stderr 
	    exit 1
	    ;;
    esac
    if test $maybe -gt 0 ; then shift ; fi 
done

random()
{
    awk 'BEGIN {srand(); printf "%f", rand()*1024; }'
}

printf "# Board,Altro,Channel,Timebin,Pedestal,Noise\n" 
for b in 0 1 16 17 ; do 
    if test $verbose -gt 0 ; then 
	printf "# --- Board 0x%02x --- \n" $b
    fi
    for a in 0 1 2 ; do 
	max_ch=15
	if test $a -eq 1 ; then max_ch=8 ; fi 
	
	if test $verbose -gt 0 ; then 
	    printf "# --- Board 0x%02x, ALTRO 0x%x (channel 0 - %d) --- \n" \
		$b $a $max_ch
	fi
	for c in `seq 0 $max_ch` ; do 
	    
	    if test $verbose -gt 0 ; then 
		printf "# --- Board 0x%02x, ALTRO 0x%x, Channel 0x%02x --- \n"\
		    $b $a $c
	    fi
	    base_adc=$value
	    for t in `seq $start $end` ; do 
		let tt=$t+14 

		ped=$base_adc
		noi=$noise
		case $type in 
		    flat) ;; 
		    incr*) let ped=$base_adc+$t ;; 
		    ran*) ped=`random` ;; 
		    ch*)  ped=$c ;;
		    *) ;;
		esac
		
		printf "%d,%d,%d,%d,%f,%f\n" $b $a $c $tt $ped $noise
	    done
	done
    done
done
echo "# EOF"

			
			
		    
	    
