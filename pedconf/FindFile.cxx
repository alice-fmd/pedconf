#include <string>
#include <fstream>
#include <sstream>
#include <vector>
#include <iostream>

//____________________________________________________________________
bool
TryOpenFile(const std::string& file, bool verbose)
{
  if (verbose) 
    std::cout << "Will try to open the file '" << file << "' ..." << std::flush;
  std::ifstream in(file.c_str());
  if (!in) { 
    if (verbose) std::cout << " failed" << std::endl;
    return false;
  }
  if (verbose) std::cout << " succeeded" << std::endl;
  in.close();
  return true;
}

//____________________________________________________________________
bool
FindFile(std::string& file, std::string& path, bool verbose=false)
{
  if (file.empty()) return false;
  

  if (file[0] == '/')  {
    if (verbose) 
      std::cout << "Absolute path given, stripping file and directory " 
		<< "and appended to the path" << std::endl;
    // Note, that we try the absolute path LAST. 
    // if (TryOpenFile(file,verbose)) return true;
    size_t      slash =  file.find_last_of("/");
    std::string dir   =  file.substr(0, slash);
    file              =  file.substr(slash+1);
    path              += ":" + dir;
    if (verbose) 
      std::cout << "File not found at absolute path, checking for '" 
		<< file << "' in path '" << path << "'" << std::endl;
  }
  
  std::stringstream s(path);
  std::vector<std::string> dirs;
  
  while (!s.eof()) { 
    std::string token;
    std::getline(s, token, ':');
    if (token.empty()) continue;
    
    if (verbose) 
      std::cout << "Adding '" << token << "' to search path" << std::endl;
    dirs.push_back(token);
  }

  for (size_t i = 0; i < dirs.size(); i++) { 
    std::string test(dirs[i]);
    test += '/';
    test += file;
    
    if (!TryOpenFile(test,verbose)) continue;
    file = test;
    return true;
  }
  return false;
}
//____________________________________________________________________
//
// EOF
//

