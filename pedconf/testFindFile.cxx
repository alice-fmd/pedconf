#include "FindFile.cxx"
#include <confdaemon/Options.h>
#include <iostream>

int
main(int argc, char** argv)
{
  Option<bool>         hOpt('h', "help",      "\tThis help", false, false); 
  Option<bool>         vOpt('v', "verbose",   "\tBe verbose",false,false);
  Option<std::string>  iOpt('i', "input",     "\tInput file\t",
			    "/etc/fstab");
  Option<std::string>  POpt('P', "path",      "\tSearch path\t",
			    "/lib:/var");
  CommandLine cl("SOURCE");
  cl.Add(hOpt);
  cl.Add(vOpt);
  cl.Add(iOpt);
  cl.Add(POpt);

  if (!cl.Process(argc, argv)) return 1;
  if (hOpt.IsSet()) {
    cl.Help();
    return 0;
  }
  std::string file = iOpt;
  std::string path = POpt;
  
  if (!FindFile(file, path, vOpt)) { 
    std::cerr << "File " << file << " not found in " << path 
	      << std::endl;
    return 1;
  }

  std::cout << "Found " << file << " search path was " << path << std::endl;

  return 0;
}

