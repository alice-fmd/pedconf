// -*- mode: C++ -*- 
#ifndef PEDCONF_PEDLOADER_H
#define PEDCONF_PEDLOADER_H
#include <iosfwd>
#include <vector>
#include <map>
#include <string>
#include <pedconf/PedLine.h>
#include <pedconf/PedTimebin.h>

namespace Rcuxx
{
  class Rcu;
  class Altro;
  class RcuRMEM;
}

namespace PedConf
{

  //==================================================================
  /** 
   * Class to load pedestals onto front-end 
   * 
   * This class will load data from an input stream and store the read
   * data in objects of class PedLine.  Then, depending on options, it
   * will 
   * 
   * - Load the pedestals onto the front-end (optionally disabled by
   *   the no-load flag).
   * - For each write, check the return code (see SetCheck)
   * - Verify that the data written to the front-end is correctly set
   *   by reading back all the written values (see also SetVerify) 
   *
   */
  class PedLoader 
  {
  public:
    /** 
     * Constructor 
     * @param rcu Rcu interface 
     * @param altro ALTRO interface 
     * @param input Stream to read values from 
     */
    PedLoader(Rcuxx::Rcu& rcu, Rcuxx::Altro& altro, std::istream& fInput);
    /** 
     * run the loader 
     *
     * @return 0 on success, error code otherwise 
     */ 
    int Run();

    /** 
     * @{ 
     * @name Settings
     */
    /** 
     * Set debug flag 
     *
     * @param on Whether to turn on or off
     */
    void SetDebug(bool on=true) { fDebug = on; }
    /** 
     * Set verify flag 
     *
     * @param on Whether to turn on or off
     */
    void SetVerify(bool on=true) { fVerify = on; }
    /** 
     * Set checking flag 
     *
     * @param on Whether to turn on or off
     */
    void SetCheck(bool on=true) { fChecks = on; }
    /** 
     * Set noise factor 
     * 
     * @param f For each timebin written to the front end is given by 
     *          @f$ v = p + f \delta@f$ where @f$ p@f$ is the measured
     *          pedestal, @f$ \delta@f$ is the measured noise, and
     *          @f$ f@f$ is the factor passed to this member
     *          function. 
     */
    void SetFactor(float f=3) { fFactor = f; }
    /** 
     * Set the no-load option 
     *
     * @param on Whether to turn on or off
     */
    void SetNoload(bool on=true) { fNoload = on; }
    /**
     * Set the first timebin to fill 
     *
     * @param off The first timebin to consider 
     */
    void SetTOffset(unsigned short off=14) { fTOffset = off; }
    /** 
     * Set whether to show warnings about missing/disabled channels
     * present in the input file. 
     *
     * @param on Whether to turn on or off
     */
    void SetShowDisabled(bool on=true) { fShowDisabled = on; }
    /** 
     * Set the maximum number of IMEM lines to write at a time.
     * 
     * @param max Maximum number of IMEM lines to write.
     */    
    void SetMaxLines(unsigned int max=2048) { fMaxLines = max; }
    void PrintSettings() const;
    /** @} */

    /** 
     * @{ 
     * @name Logging and messages 
     */
    /** 
     * Return a string with current time 
     * 
     * @return Current time 
     */
    std::string Timestamp() const;
    /** 
     * Report an error 
     * 
     * @param msg Message format 
     */ 
    void Error(const char* msg, ...);
    /** 
     * Report a warning
     * 
     * @param msg Message format 
     */ 
    void Warn(const char* msg, ...);
    /** 
     * Report information 
     * 
     * @param msg Message format 
     */ 
    void Info(const char* msg, ...);
    /** @} */
  protected:
    /** 
     * Read in the data from the input 
     *
     * @return 0 on success, error code otherwise 
     */ 
    int ReadInput();
    /** 
     * Write the pedestals to the front-end 
     *
     * @return 0 on success, error code otherwise 
     */ 
    int WritePeds();
    /** 
     * Execute the IMEM, and check RMEM 
     *
     * @return 0 on success, error code otherwise 
     */
    int ExecIMEM(bool rnw, int j);
    /** 
     * Write the pedestals to the front-end 
     *
     * @return 0 on success, error code otherwise 
     */ 
    int Verify();
    /** 
     * Calculate a hardware index 
     * 
     * @param board   Board number 
     * @param altro   ALTRO number
     * @param channel Channel number
     * 
     * @return Index 
     */
    int HWIndex(unsigned short board, unsigned short altro, 
		unsigned short channel) const;

    /** Run number read from file */
    int fRunNo;
    /** Reference to RCU interface */
    Rcuxx::Rcu& fRcu;
    /** Reference to ALTRO interface */ 
    Rcuxx::Altro& fAltro;
    /** Reference to input stream */
    std::istream& fInput;
    /** Noise factor */ 
    float fFactor;
    /** Debug flag */ 
    bool fDebug;
    /** Whether to run verification */ 
    bool fVerify;
    /** Whether to run checks on return */ 
    bool fChecks;
    /** Whether to show disabled time-bins */
    bool fShowDisabled;
    /** Whether to skip loading */
    bool fNoload;
    /** Number of timebins to leave untouched */
    unsigned short fTOffset;
    /** Maximum number of "lines" */ 
    unsigned int fMaxLines;

    /** Type of vector of PedLine */ 
    typedef std::map<unsigned int, PedTimebin*> PedVector;
    /** Type of iterator over pedestal vector */
    typedef PedVector::iterator PedIter;
    /** Vector of pedesal values */
    PedVector fPeds;
    /** Current first iterator */ 
    PedIter fFirst;
    /** Current last iterator */
    PedIter fLast;
    /** An array of ushorts */
    typedef std::vector<unsigned short> UShortVector;
    /** Least timebin seen */ 
    UShortVector fMinTimebin;
    /** Largest timebin seen */ 
    UShortVector fMaxTimebin;

    /** Sizes */ 
    enum { 
      kNBoard = 4, 
      kNAltro = 3,
      kNChannel = 16
    };
  };

  inline int
  PedLoader::HWIndex(unsigned short board, 
		     unsigned short altro, 
		     unsigned short channel) const
  {
    unsigned short lb  = board + (board >= 16 ? -16+2 : 0);
    int            idx =  (channel + kNChannel * (altro + kNAltro * lb));
    if (idx > kNBoard * kNAltro * kNChannel) return -1;
    return idx;
  }
  
}


namespace std
{
  std::ostream& 
  operator<<(std::ostream& o, PedConf::PedLine& p);
}

#endif
// 
// EOF
//
