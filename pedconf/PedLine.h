#ifndef PEDCONF_PEDLINE
#define PEDCONF_PEDLINE
#include <string>

namespace Rcuxx
{
  class Rcu;
  class Altro;
  class RcuRMEM;
}

namespace PedConf
{
  /**
   * A container of pedestal values for a specific timebin for a given
   * board and chip combination.  Since th PMADD register is shared
   * amoung the channels of the ALTRO, we can optimise the write by
   * writting the PMADD only once for each enabled channel in a
   * particular chip.  Objects of this class contains the differnet
   * values for each channel, and contains code to write, read, and
   * check the result of those, to the hardware via the ALTRO
   * interface class of Rcu++. 
   * 
   */
  struct PedLine 
  {
    /** 
     * Constructor 
     * 
     * @param b Board number 
     * @param c Channel number 
     * @param t Timebin
     */
    PedLine(unsigned short b=0, unsigned short c=0, unsigned short t=0);
    /** 
     * Copy constructor
     * 
     * @param p Object to copy from 
     */    
    PedLine(const PedLine& p);
    /** 
     * Assignment operator
     * 
     * @param p Object to assign from. 
     * 
     * @return Reference to this object. 
     */    
    PedLine& operator=(const PedLine& p);
    /** 
     * Reset
     */    
    void Reset();
    /** 
     * Add a channel to this group.
     * 
     * @param c Channel to add
     * @param ped  Pedestal value 
     */    
    void AddChannel(unsigned short c, unsigned short ped);
    /** 
     * Make a set of write lines
     * 
     * @param altro Altro interface
     * 
     * @return Number of lines written to IMEM, negative in case of
     *         errors. 
     */    
    int Write(Rcuxx::Altro& altro);
    /** 
     * Make a set of read lines
     * 
     * @param altro Altro interface
     * 
     * @return Number of lines written to IMEM, negative in case of
     *         errors. 
     */    
    int Read(Rcuxx::Altro& altro);
    /** 
     * Check the return code of a write or read
     * 
     * @param rnw   Read-not-write flag
     * @param altro ALTRO interface
     * @param rmem  RMEM interface
     * @param off   Offset, on return new offset.
     * 
     * @return 
     */
    int CheckReturn(bool rnw, Rcuxx::Altro& altro, 
		    Rcuxx::RcuRMEM& rmem, unsigned int& off);
    /** 
     * Name 
     * 
     * @return Name
     */
    const char* Name() const;
    /** 
     * Static member function to make a hash value of this.  Used for
     * sorting. 
     * 
     * @param p Reference to pedestal line
     * 
     * @return hash value
     */
    static unsigned int Hash(const PedLine& p);
    /** 
     * Static member function to make a hash value of this.  Used for
     * sorting. 
     * 
     * @param b Board
     * @param c Channel
     * @param t Timebin
     * 
     * @return Hash value
     */
    static unsigned int Hash(unsigned short b, unsigned short c,
			     unsigned short t);
    /** Board number */
    unsigned short fBoard;
    /** Chip number */
    unsigned short fChip;
    /** Timebin number */
    unsigned short fTimebin;
    /** Bit mask of channels */
    unsigned short fChannels;
    /** Pedestal values for each channel enabled in mask */
    unsigned short fPed[16];
    /** Cache of name */
    mutable std::string fName;
  };
}
#endif
