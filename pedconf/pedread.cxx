#include "config.h"
#include <confdaemon/Options.h>
#include <rcuxx/Rcu.h>
#include <rcuxx/rcu/RcuACL.h>
#include <rcuxx/rcu/RcuACTFEC.h>
#include <rcuxx/Altro.h>
#include <rcuxx/altro/AltroPMADD.h>
#include <rcuxx/altro/AltroPMDTA.h>
#include <stdexcept>
#include <iostream>
#include <iomanip>
#ifndef VERSION
# define VERSION "?.?"
#endif


//____________________________________________________________________
int
main(int argc, char** argv)
{
  Option<bool>         hOpt('h', "help",      "\tThis help", false, false); 
  Option<bool>         vOpt('v', "version",   "\tShow version",false,false);
  Option<std::string>  dOpt('d', "debug",     "\tTurn on debug messages");
  Option<bool>         eOpt('e', "emulation", "\tEmulation",false,false);
  Option<unsigned int> OOpt('O', "offset",    "\tFirst timebin offset",15);
  Option<float>        FOpt('F', "factor",    "\tNoise factor",3);
  CommandLine cl("SOURCE");
  cl.Add(hOpt);
  cl.Add(vOpt);
  cl.Add(dOpt);
  cl.Add(eOpt);
  cl.Add(OOpt);
  cl.Add(FOpt);
  if (!cl.Process(argc, argv)) return 1;
  if (hOpt.IsSet()) {
    cl.Help();
    Rcuxx::Rcu::PrintHelp(std::cout);
    return 0;
  }
  if (vOpt.IsSet()) {
    std::cout << "load_peds, Rcu++ version " << VERSION << std::endl;
    return 0;
  }

  std::string device = (cl.Remain().size() > 0 ? cl.Remain()[0] : "");
  if (device.empty()) { 
    std::cerr << "No RCU interface specified, try " << argv[0] 
	      << " --help" << std::endl;
    return 1;
  }

  bool rcuxx_debug = dOpt->find("rcuxx")!=std::string::npos;
  bool bkend_debug = dOpt->find("backend")!=std::string::npos;
  Rcuxx::Rcu* rcu = Rcuxx::Rcu::Open(device.c_str(), eOpt.IsSet(),
				     rcuxx_debug);
  try {
    if (!rcu) throw std::runtime_error("Failed to open device");
    if (rcuxx_debug) rcu->SetDebug(Rcuxx::Rcu::kRcu, 1);
    if (bkend_debug) rcu->SetDebug(Rcuxx::Rcu::kBackend,1);

    Rcuxx::Altro       altro(*rcu);
    Rcuxx::AltroPMADD* pmadd  = altro.PMADD();
    Rcuxx::AltroPMDTA* pmdta  = altro.PMDTA();
    Rcuxx::RcuACL*     acl    = rcu->ACL();
    Rcuxx::RcuACTFEC*  actfec = rcu->ACTFEC();
    if (!acl)    throw std::runtime_error("Failed to get ACL interface");
    if (!actfec) throw std::runtime_error("Failed to get AFL interface");

    int ret = 0;
    if ((ret = actfec->Update())) throw ret;
    if ((ret = acl->Update()))    throw ret;

    for (size_t i = 0; i < 32; i++) { 
      if (!actfec->IsOn(i)) continue;
      
      std::cout << "Board # 0x" 
		<< std::hex     << std::setfill('0') 
		<< std::setw(2) << i
		<< std::dec     << std::setfill(' ') << std::endl;

      for (size_t j = 0; j < 8; j++) {
	unsigned int mask = acl->CheckChip(i, j);
	if (mask == 0) continue;

	std::cout << " Altro # 0x" 
		  << std::hex     << std::setfill('0') 
		  << std::setw(1) << j
		  << std::dec     << std::setfill(' ')  << std::endl;
	
	for (size_t k = 0; k < 16; k++) { 
	  if (!acl->CheckChannel(i, j, k)) continue;
	  
	  std::cout << "    Channel # 0x" 
		    << std::hex     << std::setfill('0') 
		    << std::setw(1) << k
		    << std::dec     << std::setfill(' ') << std::endl;

	  pmadd->SetAddress(i, j, k);
	  
	  for (size_t l = 0; l < 1024; l++) { 
	    pmadd->SetPMADD(l);
	    if ((ret = pmadd->Commit())) throw ret;
	    if ((ret = pmdta->Update())) throw ret;

	    if (l % 8 == 0) 
	      std::cout << "        " << std::setw(4) << l << ": ";
	    std::cout << " 0x" 
		      << std::setfill('0') << std::hex 
		      << std::setw(3)      << pmdta->PMDTA() 
		      << std::setfill(' ') << std::dec;
	    if (l % 8 == 7) std::cout << std::endl;
	  } // for (size_t l ...)
	} // for (size_t k ...)
      } // for (size_t j ...)
    } // for (size_t i ...)
  }
  catch (std::exception& e) {
    std::cerr << e.what() << std::endl;
    return 1;
  }
  catch (int ret) {
    std::cerr << "Error # " << std::setw(3) << ret << ": ";
    if (rcu) std::cerr << rcu->ErrorString(-ret);
    std::cerr << std::endl;
    return 1;
  }
  catch (unsigned int ret) {
    std::cerr << "Error # " << std::setw(3) << ret << ": ";
    if (rcu) std::cerr << rcu->ErrorString(ret);
    std::cerr << std::endl;
    return 1;
  }
  
  return 0;
}
//
// EOF
// 

