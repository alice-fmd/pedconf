// -*- mode: C++ -*-
#ifndef PEDCONF_PEDTIMEBIN
#define PEDCONF_PEDTIMEBIN
#include <string>
#include <map>

namespace Rcuxx
{
  class Rcu;
  class Altro;
  class AltroPMDTA;
  class RcuRMEM;
}

namespace PedConf
{
  //==================================================================
  struct PedChannel 
  {
    PedChannel(unsigned short b=0, unsigned short a=0, 
	       unsigned short c=0, unsigned short p=0);
    PedChannel(const PedChannel& p);
    PedChannel& operator=(const PedChannel& p);
    /** 
     * Make a set of write lines
     * 
     * @param altro Altro interface
     * 
     * @return Number of lines written to IMEM, negative in case of
     *         errors. 
     */    
    int Write(Rcuxx::AltroPMDTA& altro);
    /** 
     * Make a set of read lines
     * 
     * @param altro Altro interface
     * 
     * @return Number of lines written to IMEM, negative in case of
     *         errors. 
     */    
    int Read(Rcuxx::AltroPMDTA& altro);
    /** 
     * Check the return code of a write or read
     * 
     * @param rnw   Read-not-write flag
     * @param altro ALTRO interface
     * @param rmem  RMEM interface
     * @param off   Offset, on return new offset.
     * 
     * @return 
     */
    int CheckReturn(bool rnw, Rcuxx::AltroPMDTA& altro, 
		    Rcuxx::RcuRMEM& rmem, unsigned int& off);
    unsigned short GetAddress() const {return Address(fBoard, fChip, fChannel);}
    static short int Address(unsigned short b, 
				unsigned short a, 
				unsigned short c) 
    {
      return (((b & 0x1F) << 7) | ((a & 0x7) << 4) | (c & 0xF));
    }
    /** The channel number */ 
    unsigned short fBoard;
    /** The channel number */ 
    unsigned short fChip;
    /** The channel number */ 
    unsigned short fChannel;
    /** The pedestal value */
    unsigned short fPed;
    /** Static debug */
    static bool fDebug;
  };
  
  //==================================================================
  struct PedTimebin
  {
    /** 
     * Constructor
     * 
     * @param timebin 
     * 
     */    
    PedTimebin(unsigned short timebin=0);
    /** 
     * Copy constructor
     * 
     * @param other Object to copy from.
     */
    PedTimebin(const PedTimebin& other);
    /** 
     * Assignment operator
     * 
     * @param other Obect to assign from.
     * 
     * @return Reference to this.
     */    
    PedTimebin& operator=(const PedTimebin& other);
    /** 
     * Add a channel pedestal for this timebin.
     * 
     * @param board 
     * @param chip 
     * @param channel 
     * @param ped 
     */    
    void AddChannel(unsigned short board,   unsigned short chip, 
		    unsigned short channel, unsigned short ped);
    /** 
     * Check if we have an entry for a board
     * 
     * @param board Board to check for 
     * 
     * @return @c true in case of success. 
     */    
    bool HasChannel(unsigned short board, unsigned short chip, 
		  unsigned short channel) const;
    /** 
     * Check if we have an entry for a board
     * 
     * @param board Board to check for 
     * 
     * @return @c true in case of success. 
     */    
    PedChannel* GetChannel(unsigned short board, unsigned short chip, 
			   unsigned short channel);
    /** 
     * Reset the pedestals 
     * 
     */
    void Reset();
    /** 
     * Zero all pedestals 
     *
     */ 
    void Zero();
    /** 
     * Make a set of write lines
     * 
     * @param altro Altro interface
     * 
     * @return Number of lines written to IMEM, negative in case of
     *         errors. 
     */    
    int Write(Rcuxx::Altro& altro, unsigned short off);
    /** 
     * Make a set of read lines
     * 
     * @param altro Altro interface
     * 
     * @return Number of lines written to IMEM, negative in case of
     *         errors. 
     */    
    int Read(Rcuxx::Altro& altro, unsigned short off);
    /** 
     * Check the return code of a write or read
     * 
     * @param rnw   Read-not-write flag
     * @param altro ALTRO interface
     * @param rmem  RMEM interface
     * @param off   Offset, on return new offset.
     * 
     * @return 
     */
    int CheckReturn(bool rnw, Rcuxx::Altro& altro, 
		    Rcuxx::RcuRMEM& rmem, unsigned int& off,
		    unsigned short toff);
    enum { 
      kMaxBoards   = 32,
      kMaxChips    =  8, 
      kMaxChannels = 16
    };
	
      
    /** The timebin */
    unsigned short fTimebin;
    /** Map of boards */ 
    typedef std::map<unsigned short,PedChannel*> ChannelMap;
    /** Map of boards */ 
    ChannelMap fChannels;
    /** Static debug */
    static bool fDebug;
  };
}
#endif
//
// EOF
//
